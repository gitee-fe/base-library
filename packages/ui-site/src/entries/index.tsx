import {render} from 'react-dom';
import {HashRouter} from 'react-router-dom';
import App from '@/components/App';
import '@/styles';

render(
    <HashRouter>
        <App />
    </HashRouter>,
    document.body.appendChild(document.createElement('div'))
);
