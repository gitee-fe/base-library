import {FC} from 'react';
import {Breadcrumb} from '@gitee/ui';
import Section from '@/components/Section';
import Row from '@/components/Row';

const BreadcrumbPreview: FC = () => (
    <Section title="Breadcrumb">
        <Row>
            <Breadcrumb>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a>Application Center</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a>Application List</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>An Application</Breadcrumb.Item>
            </Breadcrumb>
        </Row>
    </Section>
);

export default BreadcrumbPreview;
