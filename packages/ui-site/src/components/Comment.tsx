import {FC, createElement, useReducer, useCallback, useMemo} from 'react';
import {Comment, Avatar, Tooltip} from '@gitee/ui';
import {DislikeOutlined, LikeOutlined, DislikeFilled, LikeFilled} from '@ant-design/icons';
import moment from 'moment';
import Section from '@/components/Section';
import Row from '@/components/Row';

type Like = 'liked' | 'disliked' | '';

interface LikeState {
    likes: number;
    dislikes: number;
    action: Like;
}

const CommentPreview: FC = () => {
    const [{likes, dislikes, action}, dispatch] = useReducer(
        (state: LikeState, action: 'liked' | 'disliked'): LikeState => {
            if (action === state.action) {
                return state;
            }

            switch (action) {
                case 'liked':
                    return {
                        likes: state.likes + 1,
                        dislikes: state.dislikes - 1,
                        action: 'liked',
                    };
                case 'disliked':
                    return {
                        likes: state.likes - 1,
                        dislikes: state.dislikes + 1,
                        action: 'disliked',
                    };
                default:
                    return state;
            }
        },
        {likes: 172, dislikes: 35, action: ''}
    );
    const like = useCallback(() => dispatch('liked'), []);
    const dislike = useCallback(() => dispatch('disliked'), []);
    const actions = useMemo(
        () => [
            <span key="comment-basic-like">
                <Tooltip title="Like">
                    {createElement(action === 'liked' ? LikeFilled : LikeOutlined, {onClick: like})}
                </Tooltip>
                <span className="comment-action">{likes}</span>
            </span>,
            <span key=' key="comment-basic-dislike"'>
                <Tooltip title="Dislike">
                    {createElement(action === 'liked' ? DislikeFilled : DislikeOutlined, {onClick: dislike})}
                </Tooltip>
                <span className="comment-action">{dislikes}</span>
            </span>,
            <span key="comment-basic-reply-to">Reply to</span>,
        ],
        [action, dislike, dislikes, like, likes]
    );

    return (
        <Section title="Comment">
            <Row>
                <Comment
                    actions={actions}
                    author={<a>Han Solo</a>}
                    avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" alt="Han Solo" />
                    }
                    content={
                        <p>
                            We supply a series of design principles, practical patterns and high quality design
                            resources (Sketch and Axure), to help people create their product prototypes beautifully and
                            efficiently.
                        </p>
                    }
                    datetime={
                        <Tooltip title={moment().format('YYYY-MM-DD HH:mm:ss')}>
                            <span>{moment().fromNow()}</span>
                        </Tooltip>
                    }
                />
            </Row>
        </Section>
    );
};

export default CommentPreview;
