import {FC} from 'react';
import {Result, Button} from '@gitee/ui';
import Section from '@/components/Section';
import Row from '@/components/Row';

const ResultPreview: FC = () => (
    <Section title="Result">
        <Row>
            <Result
                status="success"
                title="Successfully Purchased Cloud Server BCC!"
                subTitle="Order number: 2017182818828182881 Cloud server configuration takes 1-5 minutes, please wait."
                extra={[
                    <Button type="primary" key="console">
                        Go Console
                    </Button>,
                    <Button key="buy">Buy Again</Button>,
                ]}
            />
        </Row>
    </Section>
);

export default ResultPreview;
