import {FC} from 'react';
import {Cascader} from '@gitee/ui';
import {useSize, useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';

const options = [
    {
        value: 'zhejiang',
        label: 'Zhejiang',
        children: [
            {
                value: 'hangzhou',
                label: 'Hangzhou',
                children: [
                    {
                        value: 'xihu',
                        label: 'West Lake',
                    },
                ],
            },
        ],
    },
    {
        value: 'jiangsu',
        label: 'Jiangsu',
        disabled: true,
        children: [
            {
                value: 'nanjing',
                label: 'Nanjing',
                children: [
                    {
                        value: 'zhonghuamen',
                        label: 'Zhong Hua Men',
                    },
                ],
            },
        ],
    },
];

const CascaderPreview: FC = () => {
    const size = useSize();
    const disabled = useDisabled();

    return (
        <Section title="Cascader">
            <Row>
                <Cascader options={options} size={size} disabled={disabled} style={{width: 300}} />
            </Row>
        </Section>
    );
};

export default CascaderPreview;
