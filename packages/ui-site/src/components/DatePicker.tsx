import {FC} from 'react';
import {DatePicker} from '@gitee/ui';
import {useSize, useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';

const {MonthPicker, RangePicker, WeekPicker} = DatePicker;

const DatePickerPreview: FC = () => {
    const size = useSize();
    const disabled = useDisabled();

    return (
        <Section title="DatePicker">
            <Row>
                <DatePicker disabled={disabled} size={size} />
            </Row>
            <Row>
                <MonthPicker placeholder="Select month" disabled={disabled} size={size} />
            </Row>
            <Row>
                <RangePicker disabled={disabled} size={size} />
            </Row>
            <Row>
                <WeekPicker placeholder="Select week" disabled={disabled} size={size} />
            </Row>
        </Section>
    );
};

export default DatePickerPreview;
