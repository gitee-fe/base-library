import {FC} from 'react';
import {EditOutlined, EllipsisOutlined, SettingOutlined} from '@ant-design/icons';
import {Card, Avatar} from '@gitee/ui';
import {useSize} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';
import Column from '@/components/Column';

const {Meta} = Card;

const CardPreview: FC = () => {
    const sizeInConfiguration = useSize();
    const size = sizeInConfiguration === 'small' ? 'small' : 'default';

    return (
        <Section title="Card">
            <Row>
                <Column>
                    <Card size={size} title="Default size card" extra={<a>More</a>} style={{width: 300}}>
                        <p>Card content</p>
                        <p>Card content</p>
                        <p>Card content</p>
                    </Card>
                </Column>
                <Column>
                    <Card size={size} title="Small size card" extra={<a>More</a>} style={{width: 300}}>
                        <p>Card content</p>
                        <p>Card content</p>
                        <p>Card content</p>
                    </Card>
                </Column>
            </Row>
            <Row>
                <Column>
                    <Card
                        style={{width: 300}}
                        cover={
                            <img
                                alt="example"
                                src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                            />
                        }
                        actions={[
                            <SettingOutlined key="setting" />,
                            <EditOutlined key="edit" />,
                            <EllipsisOutlined key="ellipsis" />,
                        ]}
                    >
                        <Meta
                            avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                            title="Card title"
                            description="This is the description"
                        />
                    </Card>
                </Column>
                <Column>
                    <Card style={{width: 300, marginTop: 16}} loading>
                        <Meta
                            avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                            title="Card title"
                            description="This is the description"
                        />
                    </Card>
                </Column>
            </Row>
        </Section>
    );
};

export default CardPreview;
