import {FC, useState, useCallback} from 'react';
import {Transfer} from '@gitee/ui';
import {useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';

const mockData = Array.from({length: 20}, (v, i) => {
    return {
        key: i.toString(),
        title: `content${i + 1}`,
        description: `description of content${i + 1}`,
        disabled: i % 3 < 1,
    };
});

const originalTargetKeys = mockData.filter(item => +item.key % 3 > 1).map(item => item.key);

const TransferPreview: FC = () => {
    const disabled = useDisabled();
    const [targetKeys, setTargetKeys] = useState(originalTargetKeys);
    const [selectedKeys, setSelectedKeys] = useState<string[]>([]);
    const handleChange = useCallback(targetKeys => setTargetKeys(targetKeys), []);
    const handleSelectChange = useCallback(
        (sourceSelectedKeys, targetSelectedKeys) => setSelectedKeys([...sourceSelectedKeys, ...targetSelectedKeys]),
        []
    );

    return (
        <Section title="Transfer">
            <Row>
                <Transfer
                    dataSource={mockData}
                    titles={['Source', 'Target']}
                    targetKeys={targetKeys}
                    selectedKeys={selectedKeys}
                    onChange={handleChange}
                    onSelectChange={handleSelectChange}
                    // eslint-disable-next-line react/jsx-no-bind
                    render={item => item.title || ''}
                    disabled={disabled}
                />
            </Row>
        </Section>
    );
};

export default TransferPreview;
