import {FC} from 'react';
import {DownOutlined, UserOutlined} from '@ant-design/icons';
import {Menu, Dropdown} from '@gitee/ui';
import {useSize, useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';
import Column from '@/components/Column';

const menu = (
    <Menu>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer">
                1st menu item
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer">
                2nd menu item
            </a>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer">
                3rd menu item
            </a>
        </Menu.Item>
    </Menu>
);

const DropdownPreview: FC = () => {
    const size = useSize();
    const disabled = useDisabled();

    return (
        <Section title="Dropdown">
            <Row>
                <Column>
                    <Dropdown overlay={menu} disabled={disabled}>
                        <a className="ant-dropdown-link">
                            Hover me <DownOutlined />
                        </a>
                    </Dropdown>
                </Column>
                <Column>
                    <Dropdown.Button overlay={menu} icon={<UserOutlined />} size={size} disabled={disabled}>
                        Dropdown
                    </Dropdown.Button>
                </Column>
            </Row>
        </Section>
    );
};

export default DropdownPreview;
