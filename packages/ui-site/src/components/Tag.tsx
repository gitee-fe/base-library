import {FC} from 'react';
import {Tag} from '@gitee/ui';
import Section from '@/components/Section';
import Row from '@/components/Row';
import Column from '@/components/Column';

const TagPreview: FC = () => (
    <Section title="Tag">
        <Row>
            <Column>
                <Tag>Tag 1</Tag>
            </Column>
            <Column>
                <Tag>
                    <a href="https://github.com/ant-design/ant-design/issues/1862">Link</a>
                </Tag>
            </Column>
            <Column>
                <Tag closable>Tag 2</Tag>
            </Column>
            <Column>
                <Tag closable>Prevent Default</Tag>
            </Column>
        </Row>
        <Row>
            <Column>
                <Tag color="magenta">magenta</Tag>
            </Column>
            <Column>
                <Tag color="red">red</Tag>
            </Column>
            <Column>
                <Tag color="volcano">volcano</Tag>
            </Column>
            <Column>
                <Tag color="orange">orange</Tag>
            </Column>
            <Column>
                <Tag color="gold">gold</Tag>
            </Column>
            <Column>
                <Tag color="lime">lime</Tag>
            </Column>
            <Column>
                <Tag color="green">green</Tag>
            </Column>
            <Column>
                <Tag color="cyan">cyan</Tag>
            </Column>
        </Row>
        <Row>
            <Tag color="#f50">#f50</Tag>
            <Tag color="#2db7f5">#2db7f5</Tag>
            <Tag color="#87d068">#87d068</Tag>
            <Tag color="#108ee9">#108ee9</Tag>
        </Row>
    </Section>
);

export default TagPreview;
