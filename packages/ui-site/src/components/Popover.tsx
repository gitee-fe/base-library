import {FC} from 'react';
import {Popover, Button} from '@gitee/ui';
import Section from '@/components/Section';
import Row from '@/components/Row';
import Column from '@/components/Column';

const text = <span>Title</span>;
const content = (
    <div>
        <p>Content</p>
        <p>Content</p>
    </div>
);

const PopoverPreview: FC = () => (
    <Section title="Popover">
        <Row>
            <Column>
                <Popover placement="topLeft" title={text} content={content}>
                    <Button>Align edge / 边缘对齐</Button>
                </Popover>
            </Column>
            <Column>
                <Popover placement="topLeft" title={text} content={content} arrowPointAtCenter>
                    <Button>Arrow points to center / 箭头指向中心</Button>
                </Popover>
            </Column>
        </Row>
    </Section>
);

export default PopoverPreview;
