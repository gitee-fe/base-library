import {FC} from 'react';
import {Pagination} from '@gitee/ui';
import {useSize, useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';

const PaginationPreview: FC = () => {
    const size = useSize();
    const disabled = useDisabled();

    return (
        <Section title="Pagination">
            <Row>
                <Pagination size={size} disabled={disabled} defaultCurrent={1} total={50} />
            </Row>
            <Row>
                <Pagination size={size} disabled={disabled} showSizeChanger defaultCurrent={3} total={500} />
            </Row>
            <Row>
                <Pagination size={size} disabled={disabled} total={50} showSizeChanger showQuickJumper />
            </Row>
            <Row>
                <Pagination size={size} disabled={disabled} simple defaultCurrent={2} total={50} />
            </Row>
            <Row>
                <Pagination size="small" total={50} />
            </Row>
            <Row>
                <Pagination size="small" total={50} showSizeChanger showQuickJumper />
            </Row>
            <Row>
                <Pagination size="small" total={50} />
            </Row>
        </Section>
    );
};

export default PaginationPreview;
