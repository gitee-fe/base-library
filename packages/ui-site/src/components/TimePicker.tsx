import {FC} from 'react';
import {TimePicker} from '@gitee/ui';
import moment from 'moment';
import {useSize, useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';

const TimePickerPreview: FC = () => {
    const size = useSize();
    const disabled = useDisabled();

    return (
        <Section title="TimePicker">
            <Row>
                <TimePicker size={size} disabled={disabled} defaultValue={moment('00:00:00', 'HH:mm:ss')} />
            </Row>
        </Section>
    );
};

export default TimePickerPreview;
