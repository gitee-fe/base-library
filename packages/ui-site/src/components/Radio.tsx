import {FC} from 'react';
import {Radio} from '@gitee/ui';
import {useSize, useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';

const RadioPreview: FC = () => {
    const size = useSize();
    const disabled = useDisabled();

    return (
        <Section title="Radio">
            <Row>
                <Radio.Group defaultValue="banana" size={size} disabled={disabled}>
                    <Radio value={1}>Apple</Radio>
                    <Radio value={2}>Pear</Radio>
                    <Radio value={3}>Banana</Radio>
                    <Radio value={4}>Strawberry</Radio>
                </Radio.Group>
            </Row>
            <Row>
                <Radio.Group defaultValue="a" size={size} disabled={disabled}>
                    <Radio.Button value="a">Hangzhou</Radio.Button>
                    <Radio.Button value="b">Shanghai</Radio.Button>
                    <Radio.Button value="c">Beijing</Radio.Button>
                    <Radio.Button value="d">Chengdu</Radio.Button>
                </Radio.Group>
            </Row>
        </Section>
    );
};

export default RadioPreview;
