import {FC} from 'react';
import {Rate} from '@gitee/ui';
import {useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';

const RatePreview: FC = () => {
    const disabled = useDisabled();

    return (
        <Section title="Rate">
            <Row>
                <Rate defaultValue={3} disabled={disabled} />
            </Row>
            <Row>
                <span>
                    <Rate allowHalf defaultValue={2.5} tooltips={['good']} disabled={disabled} />
                    <span className="ant-rate-text">good</span>
                </span>
            </Row>
            <Row>
                <Rate defaultValue={3} character="A" disabled={disabled} />
            </Row>
        </Section>
    );
};

export default RatePreview;
