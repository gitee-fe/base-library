import {FC} from 'react';
import {notification, Button} from '@gitee/ui';
import {useSize, useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';
import Column from '@/components/Column';

type NotificationType = 'success' | 'error' | 'info' | 'warning';

const openNotificationWithIcon = (type: NotificationType) => {
    notification[type]({
        message: 'Notification Title',
        description: (
            <>
                <p>This is the content of the notification.</p>
                <p>This is the content of the notification.</p>
                <p>This is the content of the notification.</p>
            </>
        ),
    });
};

const NotificationPreview: FC = () => {
    const size = useSize();
    const disabled = useDisabled();

    /* eslint-disable react/jsx-no-bind */
    return (
        <Section title="Notification">
            <Row>
                <Column>
                    <Button size={size} disabled={disabled} onClick={() => openNotificationWithIcon('success')}>
                        success
                    </Button>
                </Column>
                <Column>
                    <Button size={size} disabled={disabled} onClick={() => openNotificationWithIcon('info')}>
                        info
                    </Button>
                </Column>
                <Column>
                    <Button size={size} disabled={disabled} onClick={() => openNotificationWithIcon('warning')}>
                        warning
                    </Button>
                </Column>
                <Column>
                    <Button size={size} disabled={disabled} onClick={() => openNotificationWithIcon('error')}>
                        error
                    </Button>
                </Column>
            </Row>
        </Section>
    );
    /* eslint-enable react/jsx-no-bind */
};

export default NotificationPreview;
