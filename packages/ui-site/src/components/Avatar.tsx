import {FC} from 'react';
import {UserOutlined} from '@ant-design/icons';
import {Avatar} from '@gitee/ui';
import {useSize} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';
import Column from '@/components/Column';

const AvatarPreview: FC = () => {
    const sizeInConfiguration = useSize();
    const size = sizeInConfiguration === 'middle' ? 'default' : sizeInConfiguration;

    return (
        <Section title="Avatar">
            <Row>
                <Column>
                    <Avatar size={size} icon={<UserOutlined />} />
                </Column>
                <Column>
                    <Avatar size={size} shape="square" icon={<UserOutlined />} />
                </Column>
            </Row>
            <Row>
                <Column>
                    <Avatar size={size} icon={<UserOutlined />} />
                </Column>
                <Column>
                    <Avatar size={size}>U</Avatar>
                </Column>
                <Column>
                    <Avatar size={size}>USER</Avatar>
                </Column>
                <Column>
                    <Avatar size={size} src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                </Column>
                <Column>
                    <Avatar size={size} style={{color: '#f56a00', backgroundColor: '#fde3cf'}}>
                        U
                    </Avatar>
                </Column>
                <Column>
                    <Avatar size={size} style={{backgroundColor: '#87d068'}} icon={<UserOutlined />} />
                </Column>
            </Row>
        </Section>
    );
};

export default AvatarPreview;
