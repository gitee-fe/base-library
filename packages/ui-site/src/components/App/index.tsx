import {FC} from 'react';
import {Route} from 'react-router-dom';
import Sidebar from '@/components/Sidebar';
import Provider from '@/components/Provider';
import Breadcrumb from '@/components/Breadcrumb';
import Dropdown from '@/components/Dropdown';
import Menu from '@/components/Menu';
import Pagination from '@/components/Pagination';
import Steps from '@/components/Steps';
import Checkbox from '@/components/Checkbox';
import Cascader from '@/components/Cascader';
import DatePicker from '@/components/DatePicker';
import Input from '@/components/Input';
import Rate from '@/components/Rate';
import Radio from '@/components/Radio';
import Switch from '@/components/Switch';
import Slider from '@/components/Slider';
import Select from '@/components/Select';
import TreeSelect from '@/components/TreeSelect';
import Transfer from '@/components/Transfer';
import TimePicker from '@/components/TimePicker';
import Avatar from '@/components/Avatar';
import Badge from '@/components/Badge';
import Comment from '@/components/Comment';
import Collapse from '@/components/Collapse';
import Carousel from '@/components/Carousel';
import Calendar from '@/components/Calendar';
import Card from '@/components/Card';
import Empty from '@/components/Empty';
import List from '@/components/List';
import Popover from '@/components/Popover';
import Statistic from '@/components/Statistic';
import Tree from '@/components/Tree';
import Tooltip from '@/components/Tooltip';
import Timeline from '@/components/Timeline';
import Tag from '@/components/Tag';
import Tabs from '@/components/Tabs';
import Table from '@/components/Table';
import Alert from '@/components/Alert';
import Drawer from '@/components/Drawer';
import Modal from '@/components/Modal';
import Message from '@/components/Message';
import Notification from '@/components/Notification';
import Progress from '@/components/Progress';
import Popconfirm from '@/components/Popconfirm';
import Result from '@/components/Result';
import Spin from '@/components/Spin';
import Skeleton from '@/components/Skeleton';
import c from './index.less';

const App: FC = () => (
    <div className={c.root}>
        <Sidebar />
        <div className={c.content}>
            <Provider>
                <main>
                    <Route path="/:component?">
                        <Breadcrumb />
                        <Dropdown />
                        <Menu />
                        <Pagination />
                        <Steps />
                        <Checkbox />
                        <Cascader />
                        <DatePicker />
                        <Input />
                        <Rate />
                        <Radio />
                        <Switch />
                        <Slider />
                        <Select />
                        <TreeSelect />
                        <Transfer />
                        <TimePicker />
                        <Avatar />
                        <Badge />
                        <Comment />
                        <Collapse />
                        <Carousel />
                        <Calendar />
                        <Card />
                        <Empty />
                        <List />
                        <Popover />
                        <Statistic />
                        <Tree />
                        <Tooltip />
                        <Timeline />
                        <Tag />
                        <Tabs />
                        <Table />
                        <Alert />
                        <Drawer />
                        <Modal />
                        <Message />
                        <Notification />
                        <Progress />
                        <Popconfirm />
                        <Result />
                        <Spin />
                        <Skeleton />
                    </Route>
                </main>
            </Provider>
        </div>
    </div>
);

export default App;
