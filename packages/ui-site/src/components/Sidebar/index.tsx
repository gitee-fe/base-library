import {FC} from 'react';
import {PicCenterOutlined, FormOutlined, DashboardOutlined, SolutionOutlined} from '@ant-design/icons';
import {SideNavigation, Category, Item, Logo} from '@gitee/side-navigation';
import collapsedLogo from './assets/logo-collapsed.svg';
import expandedLogo from './assets/logo-expanded.svg';

const Sidebar: FC = () => (
    <SideNavigation
        setting={false}
        title="组件库"
        logo={<Logo collapsedImage={collapsedLogo} expandedImage={expandedLogo} />}
        defaultState="expanded"
    >
        <Category title="Navigation" icon={<PicCenterOutlined />}>
            <Item title="Breadcrumb" to="/breadcrumb" />
            <Item title="Dropdown" to="/dropdown" />
            <Item title="Menu" to="/menu" />
            <Item title="Pagination" to="/pagination" />
            <Item title="Steps" to="/steps" />
        </Category>
        <Category title="Data Entry" icon={<FormOutlined />}>
            <Item title="Checkbox" to="/checkbox" />
            <Item title="Cascader" to="/cascader" />
            <Item title="DatePicker" to="/date-picker" />
            <Item title="InputNumber" to="/input-number" />
            <Item title="Input" to="/input" />
            <Item title="Rate" to="/rate" />
            <Item title="Radio" to="/radio" />
            <Item title="Switch" to="/switch" />
            <Item title="Slider" to="/slider" />
            <Item title="Select" to="/select" />
            <Item title="TreeSelect" to="/tree-select" />
            <Item title="Transfer" to="/transfer" />
            <Item title="TimePicker" to="/time-picker" />
        </Category>
        <Category title="Data Display" icon={<DashboardOutlined />}>
            <Item title="Avatar" to="/avatar" />
            <Item title="Badge" to="/badge" />
            <Item title="Comment" to="/comment" />
            <Item title="Collapse" to="/collapse" />
            <Item title="Carousel" to="/carousel" />
            <Item title="Calendar" to="/calendar" />
            <Item title="Card" to="/card" />
            <Item title="Empty" to="/empty" />
            <Item title="List" to="/list" />
            <Item title="Popover" to="/popover" />
            <Item title="Statistic" to="/statistic" />
            <Item title="Tree" to="/tree" />
            <Item title="Tooltip" to="/tooltip" />
            <Item title="Timeline" to="/timeline" />
            <Item title="Tag" to="/tag" />
            <Item title="Tabs" to="/tabs" />
            <Item title="Table" to="/table" />
        </Category>
        <Category title="Feedback" icon={<SolutionOutlined />}>
            <Item title="Alert" to="/alert" />
            <Item title="Drawer" to="/drawer" />
            <Item title="Modal" to="/modal" />
            <Item title="Message" to="/message" />
            <Item title="Notification" to="/notification" />
            <Item title="Progress" to="/progress" />
            <Item title="Popconfirm" to="/popconfirm" />
            <Item title="Result" to="/result" />
            <Item title="Spin" to="/spin" />
            <Item title="Skeleton" to="/skeleton" />
        </Category>
    </SideNavigation>
);

export default Sidebar;
