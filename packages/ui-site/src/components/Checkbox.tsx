import {FC} from 'react';
import {Checkbox} from '@gitee/ui';
import {useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';

const CheckboxPreview: FC = () => {
    const disabled = useDisabled();

    return (
        <Section title="Checkbox">
            <Row>
                <Checkbox.Group disabled={disabled}>
                    <Checkbox value="apple">Apple</Checkbox>
                    <Checkbox value="pear">Pear</Checkbox>
                    <Checkbox value="orange">Orange</Checkbox>
                    <Checkbox value="banana">Banana</Checkbox>
                </Checkbox.Group>
            </Row>
        </Section>
    );
};

export default CheckboxPreview;
