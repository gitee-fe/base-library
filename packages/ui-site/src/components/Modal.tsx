import {FC} from 'react';
import {Modal, Button} from '@gitee/ui';
import {useSwitch} from '@huse/boolean';
import {useSize, useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';
import Column from '@/components/Column';

const {confirm} = Modal;

const info = () => {
    Modal.info({
        title: 'This is a notification message',
        content: (
            <div>
                <p>some messages...some messages...</p>
                <p>some messages...some messages...</p>
            </div>
        ),
    });
};

const success = () => {
    Modal.success({
        title: 'This is a success message',
        content: 'some messages...some messages...',
    });
};

const error = () => {
    Modal.error({
        title: 'This is an error message',
        content: 'some messages...some messages...',
    });
};

const warning = () => {
    Modal.warning({
        title: 'This is a warning message',
        content: 'some messages...some messages...',
    });
};

const showConfirm = () => {
    confirm({
        title: 'Do you Want to delete these items?',
        content: 'Some descriptions',
    });
};

const showDeleteConfirm = () => {
    confirm({
        title: 'Are you sure delete this task?',
        content: 'Some descriptions',
        okText: 'Yes',
        okType: 'danger',
        cancelText: 'No',
    });
};

const ModalPreview: FC = () => {
    const [visible, show, hide] = useSwitch(false);
    const size = useSize();
    const disabled = useDisabled();

    return (
        <Section title="Modal">
            <Row>
                <Column>
                    <Button type="primary" onClick={show} size={size} disabled={disabled}>
                        Open Modal
                    </Button>
                </Column>
                <Column>
                    <Button onClick={showConfirm} size={size} disabled={disabled}>
                        Confirm
                    </Button>
                </Column>
                <Column>
                    <Button onClick={showDeleteConfirm} type="dashed" size={size} disabled={disabled}>
                        Delete
                    </Button>
                </Column>
            </Row>
            <Row>
                <Column>
                    <Button onClick={info} size={size} disabled={disabled}>
                        Info
                    </Button>
                </Column>
                <Column>
                    <Button onClick={success} size={size} disabled={disabled}>
                        Success
                    </Button>
                </Column>
                <Column>
                    <Button onClick={error} size={size} disabled={disabled}>
                        Error
                    </Button>
                </Column>
                <Column>
                    <Button onClick={warning} size={size} disabled={disabled}>
                        Warning
                    </Button>
                </Column>
            </Row>
            <Modal title="Basic Modal" visible={visible} onOk={hide} onCancel={hide}>
                <p>Some contents...</p>
                <p>Some contents...</p>
                <p>Some contents...</p>
            </Modal>
        </Section>
    );
};

export default ModalPreview;
