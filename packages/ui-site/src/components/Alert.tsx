import {FC} from 'react';
import {Alert} from '@gitee/ui';
import Section from '@/components/Section';
import Row from '@/components/Row';

const AlertPreview: FC = () => (
    <Section title="Alert">
        <Row>
            <Alert closable showIcon message="Success Tips" type="success" />
        </Row>
        <Row>
            <Alert closable showIcon message="Informational Notes" type="info" />
        </Row>
        <Row>
            <Alert closable showIcon message="Warning" type="warning" />
        </Row>
        <Row>
            <Alert closable showIcon message="Error" type="error" />
        </Row>
        <Row>
            <Alert
                closable
                showIcon
                message="Success Tips"
                description="Detailed description and advice about successful copywriting."
                type="success"
            />
        </Row>
        <Row>
            <Alert
                closable
                showIcon
                message="Informational Notes"
                description="Additional description and information about copywriting."
                type="info"
            />
        </Row>
        <Row>
            <Alert
                closable
                showIcon
                message="Warning"
                description="This is a warning notice about copywriting."
                type="warning"
            />
        </Row>
        <Row>
            <Alert
                closable
                showIcon
                message="Error"
                description="This is an error message about copywriting."
                type="error"
            />
        </Row>
    </Section>
);

export default AlertPreview;
