import {FC} from 'react';
import {Collapse} from '@gitee/ui';
import {useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';

const {Panel} = Collapse;

const text = `
    A dog is a type of domesticated animal.
    Known for its loyalty and faithfulness,
    it can be found as a welcome guest in many households across the world.
`;

const text1 = (
    <p style={{paddingLeft: 24}}>
        A dog is a type of domesticated animal. Known for its loyalty and faithfulness, it can be found as a welcome
        guest in many households across the world.
    </p>
);

const CollapsePreview: FC = () => {
    const disabled = useDisabled();

    return (
        <Section title="Collapse">
            <Row>
                <Collapse defaultActiveKey={['1']}>
                    <Panel header="This is panel header 1" key="1" disabled={disabled}>
                        <p>{text}</p>
                    </Panel>
                    <Panel header="This is panel header 2" key="2" disabled={disabled}>
                        <p>{text}</p>
                    </Panel>
                    <Panel header="This is panel header 3" key="3" disabled={disabled}>
                        <p>{text}</p>
                    </Panel>
                </Collapse>
            </Row>
            <Row>
                <Collapse bordered={false} defaultActiveKey={['1']}>
                    <Panel header="This is panel header 1" key="1" disabled={disabled}>
                        {text1}
                    </Panel>
                    <Panel header="This is panel header 2" key="2" disabled={disabled}>
                        {text1}
                    </Panel>
                    <Panel header="This is panel header 3" key="3" disabled={disabled}>
                        {text1}
                    </Panel>
                </Collapse>
            </Row>
        </Section>
    );
};

export default CollapsePreview;
