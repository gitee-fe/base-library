import {FC} from 'react';
import {Calendar, Badge} from '@gitee/ui';
import {Moment} from 'moment';
import Section from '@/components/Section';
import Row from '@/components/Row';

interface Item {
    type: 'success' | 'processing' | 'default' | 'error' | 'warning';
    content: string;
}

function getListData(value: Moment): Item[] {
    switch (value.date()) {
        case 8:
            return [
                {type: 'warning', content: 'This is warning event.'},
                {type: 'success', content: 'This is usual event.'},
            ];
        case 10:
            return [
                {type: 'warning', content: 'This is warning event.'},
                {type: 'success', content: 'This is usual event.'},
                {type: 'error', content: 'This is error event.'},
            ];
        case 15:
            return [
                {type: 'warning', content: 'This is warning event'},
                {type: 'success', content: 'This is very long usual event。。....'},
                {type: 'error', content: 'This is error event 1.'},
                {type: 'error', content: 'This is error event 2.'},
                {type: 'error', content: 'This is error event 3.'},
                {type: 'error', content: 'This is error event 4.'},
            ];
        default:
            return [];
    }
}

function dateCellRender(value: Moment) {
    const listData = getListData(value);
    const renderItem = (item: Item) => (
        <li key={item.content}>
            <Badge status={item.type} text={item.content} />
        </li>
    );

    return (
        <ul className="events">
            {listData.map(renderItem)}
        </ul>
    );
}

function getMonthData(value: Moment) {
    if (value.month() === 8) {
        return 1394;
    }
}

function monthCellRender(value: Moment) {
    const monthData = getMonthData(value);

    if (monthData) {
        return (
            <div className="notes-month">
                <section>{monthData}</section>
                <span>Backlog number</span>
            </div>
        );
    }

    return null;
}

const CalendarPreview: FC = () => (
    <Section title="Calendar">
        <Row>
            <Calendar dateCellRender={dateCellRender} monthCellRender={monthCellRender} />
        </Row>
        <Row>
            <Calendar fullscreen={false} />
        </Row>
    </Section>
);

export default CalendarPreview;
