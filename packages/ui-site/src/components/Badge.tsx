import {FC} from 'react';
import {ClockCircleOutlined} from '@ant-design/icons';
import {Badge} from '@gitee/ui';
import Section from '@/components/Section';
import Row from '@/components/Row';
import Column from '@/components/Column';

const BadgePreview: FC = () => (
    <Section title="Badge">
        <Row>
            <Column style={{marginRight: 40}}>
                <Badge count={5}>
                    <a className="badge-square" />
                </Badge>
            </Column>
            <Column style={{marginRight: 40}}>
                <Badge count={0} showZero>
                    <a className="badge-square" />
                </Badge>
            </Column>
            <Column style={{marginRight: 40}}>
                <Badge count={<ClockCircleOutlined style={{color: '#f5222d'}} />}>
                    <a className="badge-square" />
                </Badge>
            </Column>
            <Column style={{marginRight: 60}}>
                <Badge count={99}>
                    <a className="badge-square" />
                </Badge>
            </Column>
            <Column style={{marginRight: 70}}>
                <Badge count={100}>
                    <a className="badge-square" />
                </Badge>
            </Column>
            <Column style={{marginRight: 70}}>
                <Badge count={99} overflowCount={10}>
                    <a className="badge-square" />
                </Badge>
            </Column>
            <Column style={{marginRight: 70}}>
                <Badge count={1000} overflowCount={999}>
                    <a className="badge-square" />
                </Badge>
            </Column>
            <Column>
                <Badge dot>
                    <a className="badge-square" />
                </Badge>
            </Column>
        </Row>
        <Row>
            <Column style={{marginRight: 40}}>
                <Badge count={25} />
            </Column>
            <Column style={{marginRight: 40}}>
                <Badge
                    count={4}
                    style={{backgroundColor: '#fff', color: '#999', boxShadow: '0 0 0 1px #d9d9d9 inset'}}
                />
            </Column>
            <Column style={{marginRight: 40}}>
                <Badge count={109} style={{backgroundColor: '#52c41a'}} />
            </Column>
        </Row>
        <Row>
            <Column style={{marginRight: 40}}>
                <Badge status="success" text="Success" />
            </Column>
            <Column style={{marginRight: 40}}>
                <Badge status="error" text="Error" />
            </Column>
            <Column style={{marginRight: 40}}>
                <Badge status="default" text="Default" />
            </Column>
            <Column style={{marginRight: 40}}>
                <Badge status="processing" text="Processing" />
            </Column>
            <Column style={{marginRight: 40}}>
                <Badge status="warning" text="Warning" />
            </Column>
        </Row>
    </Section>
);

export default BadgePreview;
