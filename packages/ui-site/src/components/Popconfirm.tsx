import {FC} from 'react';
import {Popconfirm, message} from '@gitee/ui';
import Section from '@/components/Section';
import Row from '@/components/Row';

const confirm = () => {
    message.success('Click on Yes');
};

const cancel = () => {
    message.error('Click on No');
};

const PopconfirmPreview: FC = () => (
    <Section title="Popover">
        <Row>
            <Popconfirm
                title="Are you sure delete this task?"
                onConfirm={confirm}
                onCancel={cancel}
                okText="Yes"
                cancelText="No"
            >
                <a>Delete</a>
            </Popconfirm>
        </Row>
    </Section>
);

export default PopconfirmPreview;
