import {FC} from 'react';
import {Tabs} from '@gitee/ui';
import {useSize, useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';

const {TabPane} = Tabs;

const TabsPreview: FC = () => {
    const sizeInConfiguration = useSize();
    const disabled = useDisabled();
    const size = sizeInConfiguration === 'middle' ? 'default' : sizeInConfiguration;

    return (
        <Section title="Tabs">
            <Row>
                <Tabs size={size} defaultActiveKey="1">
                    <TabPane tab="Tab 1" key="1" disabled={disabled}>
                        Content of Tab Pane 1
                    </TabPane>
                    <TabPane tab="Tab 2" key="2" disabled={disabled}>
                        Content of Tab Pane 2
                    </TabPane>
                    <TabPane tab="Tab 3" key="3" disabled={disabled}>
                        Content of Tab Pane 3
                    </TabPane>
                </Tabs>
            </Row>
            <Row>
                <Tabs size={size} type="card">
                    <TabPane tab="Tab 1" key="1" disabled={disabled}>
                        Content of Tab Pane 1
                    </TabPane>
                    <TabPane tab="Tab 2" key="2" disabled={disabled}>
                        Content of Tab Pane 2
                    </TabPane>
                    <TabPane tab="Tab 3" key="3" disabled={disabled}>
                        Content of Tab Pane 3
                    </TabPane>
                </Tabs>
            </Row>
        </Section>
    );
};

export default TabsPreview;
