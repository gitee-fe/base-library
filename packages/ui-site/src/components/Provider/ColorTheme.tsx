import {FC, useEffect} from 'react';
import {Select} from '@gitee/ui';

const {Option} = Select;

const themes = {
    default: [
        '#F2F8FF',
        '#D3E9FF',
        '#A3D3FF',
        '#73BEFF',
        '#41A7FA',
        '#108CEE',
        '#0476C7',
        '#0061A1',
        '#004B7A',
        '#013454',
    ],
    green: [
        '#e8f5e9',
        '#c8e6c9',
        '#a5d6a7',
        '#81c784',
        '#66bb6a',
        '#4caf50',
        '#43a047',
        '#388e3c',
        '#2e7d32',
        '#1b5e20',
    ],
    orange: [
        '#fff3e0',
        '#ffe0b2',
        '#ffcc80',
        '#ffb74d',
        '#ffa726',
        '#ff9800',
        '#fb8c00',
        '#f57c00',
        '#ef6c00',
        '#e65100',
    ],
    purple: [
        '#f3e5f5',
        '#e1bee7',
        '#ce93d8',
        '#ba68c8',
        '#ab47bc',
        '#9c27b0',
        '#8e24aa',
        '#7b1fa2',
        '#6a1b9a',
        '#4a148c',
    ],
};

export type ThemeName = keyof typeof themes;

interface Props {
    value: ThemeName;
    onChange(name: ThemeName): void;
}

const ColorTheme: FC<Props> = ({value, onChange}) => {
    useEffect(
        () => {
            const root = document.documentElement;
            const colors = themes[value];
            for (let i = 0; i < colors.length; i++) {
                root.style.setProperty(`--color-brand-${i + 1}`, colors[i]);
            }
        },
        [value]
    );

    return (
        <Select value={value} onChange={onChange}>
            <Option value="default">Default</Option>
            <Option value="green">Green</Option>
            <Option value="orange">Orange</Option>
            <Option value="purple">Purple</Option>
        </Select>
    );
};

export default ColorTheme;
