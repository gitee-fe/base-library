import {FC, createContext, useState, useContext, useMemo} from 'react';
import {Checkbox, Radio} from '@gitee/ui';
import c from './index.less';
import ColorTheme, {ThemeName} from './ColorTheme';

export interface PreviewContext {
    disabled: boolean;
    size: 'small' | 'middle' | 'large';
}

const Context = createContext<PreviewContext>({disabled: false, size: 'middle'});
Context.displayName = 'PreviewContext';

const PreviewProvider: FC = ({children}) => {
    const [disabled, setDisabled] = useState(false);
    const [size, setSize] = useState<PreviewContext['size']>('middle');
    const [colorTheme, setColorTheme] = useState<ThemeName>('default');
    const contextValue = useMemo(() => ({size, disabled}), [size, disabled]);

    /* eslint-disable react/jsx-no-bind */
    return (
        <>
            <div className={c.form}>
                <div className={c.field}>
                    <Checkbox checked={!disabled} onChange={e => setDisabled(!e.target.checked)}>Enabled</Checkbox>
                </div>
                <div className={c.field}>
                    <span className={c.fieldTitle}>Size:</span>
                    <Radio.Group value={size} onChange={e => setSize(e.target.value)}>
                        <Radio.Button value="small">Small</Radio.Button>
                        <Radio.Button value="middle">Middle</Radio.Button>
                        <Radio.Button value="large">Large</Radio.Button>
                    </Radio.Group>
                </div>
                <div className={c.field}>
                    <span className={c.fieldTitle}>Theme:</span>
                    <ColorTheme value={colorTheme} onChange={setColorTheme} />
                </div>
            </div>
            <Context.Provider value={contextValue}>
                {children}
            </Context.Provider>
        </>
    );
    /* eslint-enable react/jsx-no-bind */
};

export default PreviewProvider;

export const useDisabled = () => {
    const {disabled} = useContext(Context);
    return disabled;
};

export const useSize = () => {
    const {size} = useContext(Context);
    return size;
};
