import {FC, useState, useCallback} from 'react';
import {Drawer, Button} from '@gitee/ui';
import Section from '@/components/Section';
import Row from '@/components/Row';
import Column from './Column';

const DrawerPreview: FC = () => {
    const [drawerType, setDrawerType] = useState<'close' | 'mask' | 'no-mask'>('close');
    const openMask = useCallback(
        () => setDrawerType('mask'),
        []
    );
    const openNoMask = useCallback(
        () => setDrawerType('no-mask'),
        []
    );
    const close = useCallback(
        () => setDrawerType('close'),
        []
    );

    return (
        <Section title="Drawer">
            <Row>
                <Column>
                    <Button type="primary" onClick={openMask}>
                        Open Drawer
                    </Button>
                </Column>
                <Column>
                    <Button onClick={openNoMask}>
                        No Mask
                    </Button>
                </Column>
                <Drawer
                    closable
                    maskClosable
                    mask={drawerType === 'mask'}
                    visible={drawerType !== 'close'}
                    onClose={close}
                    footer={<Button onClick={close}>Close</Button>}
                >
                    <p>Some contents...</p>
                    <p>Some contents...</p>
                    <p>Some contents...</p>
                </Drawer>
            </Row>
        </Section>
    );
};

export default DrawerPreview;
