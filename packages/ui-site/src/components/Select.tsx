import {FC} from 'react';
import {Select} from '@gitee/ui';
import {useSize, useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';
import Column from '@/components/Column';

const {Option, OptGroup} = Select;

const children = Array.from(
    {length: 26},
    (v, i) => (
        <Option key={(i + 10).toString(36) + i.toString()} value={i}>
            {(i + 10).toString(36) + i.toString()}
        </Option>
    )
);

const SelectPreview: FC = () => {
    const size = useSize();
    const disabled = useDisabled();

    return (
        <Section title="Select">
            <Row>
                <Column>
                    <Select defaultValue="lucy" style={{width: 160}} disabled={disabled} size={size}>
                        <Option value="jack">Jack</Option>
                        <Option value="lucy">Lucy</Option>
                        <Option value="disabled" disabled>
                            Disabled
                        </Option>
                        <Option value="Yiminghe">yiminghe</Option>
                    </Select>
                </Column>
                <Column>
                    <Select defaultValue="lucy" style={{width: 160}} disabled={disabled} size={size}>
                        <OptGroup label="Manager">
                            <Option value="jack">Jack</Option>
                            <Option value="lucy">Lucy</Option>
                        </OptGroup>
                        <OptGroup label="Engineer">
                            <Option value="Yiminghe">yiminghe</Option>
                        </OptGroup>
                    </Select>
                </Column>
            </Row>
            <Row>
                <Select
                    mode="multiple"
                    style={{width: '100%'}}
                    placeholder="Please select"
                    defaultValue={['a10', 'c12']}
                    disabled={disabled}
                    size={size}
                >
                    {children}
                </Select>
            </Row>
        </Section>
    );
};

export default SelectPreview;
