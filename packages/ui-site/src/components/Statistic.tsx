import {FC} from 'react';
import {Statistic, Button} from '@gitee/ui';
import Section from '@/components/Section';
import Row from '@/components/Row';
import Column from '@/components/Column';

const StatisticPreview: FC = () => (
    <Section title="Statistic">
        <Row style={{display: 'flex'}}>
            <Column>
                <Statistic title="Active Users" value={112893} />
            </Column>
            <Column>
                <Statistic title="Account Balance (CNY)" value={112893} precision={2} />
                <Button style={{marginTop: 16}} type="primary">
                    Recharge
                </Button>
            </Column>
        </Row>
    </Section>
);

export default StatisticPreview;
