import {FC} from 'react';
import {Input, InputNumber} from '@gitee/ui';
import {useSize, useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';
import Column from '@/components/Column';

const {Search, TextArea} = Input;

const InputPreview: FC = () => {
    const size = useSize();
    const disabled = useDisabled();

    /* eslint-disable react/jsx-no-bind */
    return (
        <Section title="Input">
            <Row>
                <Input placeholder="Basic usage" size={size} disabled={disabled} />
            </Row>
            <Row>
                <Search placeholder="input search text" size={size} disabled={disabled} />
            </Row>
            <Row>
                <Search placeholder="input search text" enterButton size={size} disabled={disabled} />
            </Row>
            <Row>
                <Column>
                    <InputNumber defaultValue={3} size={size} disabled={disabled} />
                </Column>
                <Column>
                    <InputNumber defaultValue={3.4} min={0} max={10} step={0.1} size={size} disabled={disabled} />
                </Column>
                <Column>
                    <InputNumber
                        defaultValue={1000}
                        formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        parser={value => (value ?? '').replace(/\$\s?|(,*)/g, '')}
                        size={size}
                        disabled={disabled}
                    />
                </Column>
                <Column>
                    <InputNumber
                        defaultValue={100}
                        min={0}
                        max={100}
                        formatter={value => `${value}%`}
                        parser={value => (value ?? '').replace('%', '')}
                        size={size}
                        disabled={disabled}
                    />
                </Column>
            </Row>
            <Row>
                <TextArea rows={4} disabled={disabled} />
            </Row>
        </Section>
    );
    /* eslint-enable react/jsx-no-bind */
};

export default InputPreview;
