import {FC} from 'react';
import {message, Button} from '@gitee/ui';
import {useSize, useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';
import Column from '@/components/Column';

const success = () => {
    message.success('This is a success message');
};

const error = () => {
    message.error('This is an error message');
};

const info = () => {
    message.info('This is an info message');
};

const warning = () => {
    message.warning('This is a warning message');
};

const MessagePreview: FC = () => {
    const size = useSize();
    const disabled = useDisabled();

    return (
        <Section title="Message">
            <Row>
                <Column>
                    <Button size={size} disabled={disabled} onClick={success}>
                        success
                    </Button>
                </Column>
                <Column>
                    <Button size={size} disabled={disabled} onClick={info}>
                        info
                    </Button>
                </Column>
                <Column>
                    <Button size={size} disabled={disabled} onClick={warning}>
                        warning
                    </Button>
                </Column>
                <Column>
                    <Button size={size} disabled={disabled} onClick={error}>
                        error
                    </Button>
                </Column>
            </Row>
        </Section>
    );
};

export default MessagePreview;
