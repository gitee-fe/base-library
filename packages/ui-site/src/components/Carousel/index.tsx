import {FC} from 'react';
import {Carousel} from '@gitee/ui';
import Section from '@/components/Section';
import Row from '@/components/Row';
import './index.less';

const CarouselPreview: FC = () => (
    <Section title="Carousel">
        <Row>
            <Carousel autoplay>
                <div>
                    <h3>1</h3>
                </div>
                <div>
                    <h3>2</h3>
                </div>
                <div>
                    <h3>3</h3>
                </div>
                <div>
                    <h3>4</h3>
                </div>
            </Carousel>
        </Row>
    </Section>
);

export default CarouselPreview;
