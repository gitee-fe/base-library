import {FC} from 'react';
import {Slider} from '@gitee/ui';
import {useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';

const marks = {
    0: '0°C',
    26: '26°C',
    37: '37°C',
    100: {
        style: {
            color: '#f50',
        },
        label: <strong>100°C</strong>,
    },
};

const SliderPreview: FC = () => {
    const disabled = useDisabled();

    return (
        <Section title="Slider">
            <Row>
                <Slider defaultValue={30} disabled={disabled} />
            </Row>
            <Row>
                <Slider range marks={marks} defaultValue={[26, 37]} />
            </Row>
        </Section>
    );
};

export default SliderPreview;
