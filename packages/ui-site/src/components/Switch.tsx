import {FC} from 'react';
import {Switch} from '@gitee/ui';
import {useSize, useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';
import Column from '@/components/Column';

const SwitchPreview: FC = () => {
    const sizeInConfiguration = useSize();
    const disabled = useDisabled();
    const size = sizeInConfiguration === 'small' ? 'small' : 'default';

    return (
        <Section title="Switch">
            <Row>
                <Column>
                    <Switch defaultChecked size={size} disabled={disabled} />
                </Column>
                <Column>
                    <Switch
                        checkedChildren="开"
                        unCheckedChildren="关"
                        defaultChecked
                        size={size}
                        disabled={disabled}
                    />
                </Column>
                <Column>
                    <Switch loading defaultChecked size={size} disabled={disabled} />
                </Column>
            </Row>
        </Section>
    );
};

export default SwitchPreview;
