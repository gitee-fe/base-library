import {FC} from 'react';
import {Table} from '@gitee/ui';
import {ColumnProps} from 'antd/lib/table';
import {useSize} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';

interface Item {
    key: number;
    name: string;
    age: number;
    address: string;
    description: string;
}

const columns: Array<ColumnProps<Item>> = [
    {
        title: 'Name',
        dataIndex: 'name',
        width: 150,
        render(text: string) {
            return <a>{text}</a>;
        },
        filters: [
            {
                text: 'Joe',
                value: 'Joe',
            },
            {
                text: 'Jim',
                value: 'Jim',
            },
            {
                text: 'Submenu',
                value: 'Submenu',
                children: [
                    {
                        text: 'Green',
                        value: 'Green',
                    },
                    {
                        text: 'Black',
                        value: 'Black',
                    },
                ],
            },
        ],
        onFilter(value: string, record: Item) {
            return record.name.includes(value);
        },
    },
    {
        title: 'Age',
        dataIndex: 'age',
        key: 'age',
        width: 70,
        render(text: number) {
            return <a>{text}</a>;
        },
        onFilter(value: number, record: Item) {
            return record.age === value;
        },
        sorter: true,
    },
    {
        title: 'Address',
        dataIndex: 'address',
    },
];

const data: Item[] = Array.from(
    {length: 5},
    (v, i) => {
        return {
            key: i,
            name: 'John Brown',
            age: i * 10 + 2,
            address: `New York No. ${i} Lake Park`,
            description: `My name is John Brown, I am ${i}2 years old, living in New York No. ${i} Lake Park.`,
        };
    }
);

const TablePreview: FC = () => {
    const size = useSize();

    return (
        <Section title="Table">
            <Row>
                <Table size={size} columns={columns} dataSource={data} />
            </Row>
        </Section>
    );
};

export default TablePreview;
