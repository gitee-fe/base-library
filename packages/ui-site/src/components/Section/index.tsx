import {FC, useRef} from 'react';
import {Card} from '@gitee/ui';
import {useParams} from 'react-router-dom';
import {useScrollIntoView} from '@huse/scroll-into-view';
import {paramCase} from 'change-case';
import c from './index.less';

interface Props {
    title: string;
}

const Section: FC<Props> = ({title, children}) => {
    const ref = useRef(null);
    const {component} = useParams();
    useScrollIntoView(ref, component === paramCase(title));

    return (
        <div ref={ref} className={c.root}>
            <Card title={title}>
                {children}
            </Card>
        </div>
    );
};

export default Section;
