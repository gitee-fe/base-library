import {FC} from 'react';
import {Tooltip, Button} from '@gitee/ui';
import Section from '@/components/Section';
import Row from '@/components/Row';
import Column from '@/components/Column';

const TooltipPreview: FC = () => (
    <Section title="Tooltip">
        <Row>
            <Column>
                <Tooltip placement="topLeft" title="Prompt Text">
                    <Button>Align edge / 边缘对齐</Button>
                </Tooltip>
            </Column>
            <Column>
                <Tooltip placement="topLeft" title="Prompt Text" arrowPointAtCenter>
                    <Button>Arrow points to center / 箭头指向中心</Button>
                </Tooltip>
            </Column>
        </Row>
    </Section>
);

export default TooltipPreview;
