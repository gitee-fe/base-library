import {FC} from 'react';
import {Tree} from '@gitee/ui';
import {useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';
import Column from '@/components/Column';

const {TreeNode} = Tree;

const treeData = [
    {
        title: 'parent 1',
        key: '0-0',
        children: [
            {
                title: 'parent 1-0',
                key: '0-0-0',
                disabled: true,
                children: [
                    {
                        title: 'leaf',
                        key: '0-0-0-0',
                        disableCheckbox: true,
                    },
                    {
                        title: 'leaf',
                        key: '0-0-0-1',
                    },
                ],
            },
            {
                title: 'parent 1-1',
                key: '0-0-1',
                children: [{title: <span style={{color: '#1890ff'}}>sss</span>, key: '0-0-1-0'}],
            },
        ],
    },
];

const TreePreview: FC = () => {
    const disabled = useDisabled();

    return (
        <Section title="Tree">
            <Row>
                <Column>
                    <Tree
                        checkable
                        disabled={disabled}
                        defaultExpandedKeys={['0-0-0', '0-0-1']}
                        defaultSelectedKeys={['0-0-0', '0-0-1']}
                        defaultCheckedKeys={['0-0-0', '0-0-1']}
                        treeData={treeData}
                    />
                </Column>
                <Column>
                    <Tree
                        disabled={disabled}
                        defaultExpandedKeys={['0-0-0', '0-0-1']}
                        defaultSelectedKeys={['0-0-0', '0-0-1']}
                        defaultCheckedKeys={['0-0-0', '0-0-1']}
                        treeData={treeData}
                    />
                </Column>
                <Column>
                    <Tree showLine disabled={disabled} defaultExpandedKeys={['0-0-0']}>
                        <TreeNode title="parent 1" key="0-0">
                            <TreeNode title="parent 1-0" key="0-0-0">
                                <TreeNode title="leaf" key="0-0-0-0" />
                                <TreeNode title="leaf" key="0-0-0-1" />
                                <TreeNode title="leaf" key="0-0-0-2" />
                            </TreeNode>
                            <TreeNode title="parent 1-1" key="0-0-1">
                                <TreeNode title="leaf" key="0-0-1-0" />
                            </TreeNode>
                            <TreeNode title="parent 1-2" key="0-0-2">
                                <TreeNode title="leaf" key="0-0-2-0" />
                                <TreeNode title="leaf" key="0-0-2-1" />
                            </TreeNode>
                        </TreeNode>
                    </Tree>
                </Column>
            </Row>
        </Section>
    );
};

export default TreePreview;
