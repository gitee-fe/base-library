import {FC} from 'react';
import {Spin, Alert} from '@gitee/ui';
import {useSize} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';

const SpinPreview: FC = () => {
    const sizeInConfiguration = useSize();
    const size = sizeInConfiguration === 'middle' ? 'default' : sizeInConfiguration;

    return (
        <Section title="Spin">
            <Row>
                <Spin tip="Loading..." size={size}>
                    <Alert
                        message="Alert message title"
                        description="Further details about the context of this alert."
                        type="info"
                    />
                </Spin>
            </Row>
        </Section>
    );
};

export default SpinPreview;
