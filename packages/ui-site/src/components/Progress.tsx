import {FC} from 'react';
import {Progress} from '@gitee/ui';
import {useSize} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';
import Column from '@/components/Column';

const ProgressPreview: FC = () => {
    const sizeInConfiguration = useSize();
    const size = sizeInConfiguration === 'small' ? 'small' : 'default';

    return (
        <Section title="Progress">
            <Row>
                <Progress size={size} percent={30} />
                <Progress size={size} percent={50} status="active" />
                <Progress size={size} percent={70} status="exception" />
                <Progress size={size} percent={100} />
                <Progress size={size} percent={50} showInfo={false} />
            </Row>
            <Row>
                <Column>
                    <Progress size={size} type="circle" percent={75} />
                </Column>
                <Column>
                    <Progress size={size} type="circle" percent={70} status="exception" />
                </Column>
                <Column>
                    <Progress size={size} type="circle" percent={100} />
                </Column>
                <Column>
                    <Progress size={size} type="dashboard" percent={60} successPercent={30} />
                </Column>
            </Row>
        </Section>
    );
};

export default ProgressPreview;
