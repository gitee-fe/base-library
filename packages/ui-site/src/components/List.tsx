import {FC} from 'react';
import {List, Typography, Avatar} from '@gitee/ui';
import {useSize} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';

const data = [
    'Racing car sprays burning fuel into crowd.',
    'Japanese princess to wed commoner.',
    'Australian walks 100km after outback crash.',
    'Man charged over missing wedding girl.',
    'Los Angeles battles huge wildfires.',
];

const data1 = [
    {title: 'Ant Design Title 1'},
    {title: 'Ant Design Title 2'},
    {title: 'Ant Design Title 3'},
    {title: 'Ant Design Title 4'},
];

const ListPreview: FC = () => {
    const sizeInConfiguration = useSize();
    const size = sizeInConfiguration === 'middle' ? 'default' : sizeInConfiguration;
    const renderItem = (item: string) => (
        <List.Item>
            <Typography.Text mark>[ITEM]</Typography.Text> {item}
        </List.Item>
    );
    const renderItemMeta = (item: {title: string}) => (
        <List.Item>
            <List.Item.Meta
                avatar={
                    <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                }
                title={<a href="https://ant.design">{item.title}</a>}
                description="Ant Design, a design language for background applications, is refined by Ant UED Team"
            />
        </List.Item>
    );

    return (
        <Section title="List">
            <Row>
                <List
                    size={size}
                    header={<div>Header</div>}
                    footer={<div>Footer</div>}
                    bordered
                    dataSource={data}
                    // eslint-disable-next-line react/jsx-no-bind
                    renderItem={renderItem}
                />
            </Row>
            <Row>
                <List
                    itemLayout="horizontal"
                    dataSource={data1}
                    // eslint-disable-next-line react/jsx-no-bind
                    renderItem={renderItemMeta}
                />
            </Row>
        </Section>
    );
};

export default ListPreview;
