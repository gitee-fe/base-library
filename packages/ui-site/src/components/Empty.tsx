import {FC} from 'react';
import {Empty, Button} from '@gitee/ui';
import Section from '@/components/Section';
import Row from '@/components/Row';

const EmptyPreview: FC = () => (
    <Section title="Empty">
        <Row>
            <Empty description={<span>No repos</span>}>
                <Button type="primary">Create Your First Repo</Button>
            </Empty>
        </Row>
    </Section>
);

export default EmptyPreview;
