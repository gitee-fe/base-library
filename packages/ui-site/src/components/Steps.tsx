import {FC} from 'react';
import {LoadingOutlined, SmileOutlined, SolutionOutlined, UserOutlined} from '@ant-design/icons';
import {Steps} from '@gitee/ui';
import {useSize, useDisabled} from '@/components/Provider';
import Section from '@/components/Section';
import Row from '@/components/Row';

const {Step} = Steps;

const StepPreview: FC = () => {
    const sizeInConfiguration = useSize();
    const disabled = useDisabled();
    const size = sizeInConfiguration === 'small' ? 'small' : 'default';

    return (
        <Section title="Steps">
            <Row>
                <Steps current={1} size={size}>
                    <Step title="Finished" description="This is a description." disabled={disabled} />
                    <Step title="In Progress" description="This is a description." disabled={disabled} />
                    <Step title="Waiting" description="This is a description." disabled={disabled} />
                </Steps>
            </Row>
            <Row>
                <Steps size={size}>
                    <Step status="finish" title="Login" icon={<UserOutlined />} disabled={disabled} />
                    <Step status="finish" title="Verification" icon={<SolutionOutlined />} disabled={disabled} />
                    <Step status="process" title="Pay" icon={<LoadingOutlined />} disabled={disabled} />
                    <Step status="wait" title="Done" icon={<SmileOutlined />} disabled={disabled} />
                </Steps>
            </Row>
            <Row>
                <Steps progressDot current={1} size={size}>
                    <Step title="Finished" description="This is a description." disabled={disabled} />
                    <Step title="In Progress" description="This is a description." disabled={disabled} />
                    <Step title="Waiting" description="This is a description." disabled={disabled} />
                </Steps>
            </Row>
            <Row>
                <Steps direction="vertical" current={1} size={size}>
                    <Step title="Finished" description="This is a description." disabled={disabled} />
                    <Step title="In Progress" description="This is a description." disabled={disabled} />
                    <Step title="Waiting" description="This is a description." disabled={disabled} />
                </Steps>
            </Row>
        </Section>
    );
};

export default StepPreview;
