import {FC, CSSProperties} from 'react';
import c from './index.less';

interface Props {
    style?: CSSProperties;
}

const Column: FC<Props> = ({style, children}) => (
    <div className={c.root} style={style}>
        {children}
    </div>
);

export default Column;
