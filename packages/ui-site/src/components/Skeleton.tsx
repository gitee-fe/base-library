import {FC} from 'react';
import {Skeleton} from '@gitee/ui';
import Section from '@/components/Section';
import Row from '@/components/Row';

const SkeletonPreview: FC = () => (
    <Section title="Skeleton">
        <Row>
            <Skeleton avatar paragraph={{ rows: 4 }} />
        </Row>
        <Row>
            <Skeleton active />
        </Row>
    </Section>
);

export default SkeletonPreview;
