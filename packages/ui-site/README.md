# UI Site

简单展示所有的UI组件，用于查看样式定制效果。

## 启动站点

```shell
yarn start
```

## 构建

```shell
yarn build
```

构建产物在`dist/`目录下，以`index-stable.html`为入口，如有必要的话，把`index-stable.html`重命名为`index.html`再发布。
