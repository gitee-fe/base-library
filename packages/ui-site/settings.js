const {ui} = require('@gitee/reskript-plugins');

exports.build = {
    extractCSS: false,
};

exports.addition = () => {
    if (process.env.PUBLIC_PATH) {
        return {
            output: {
                publicPath: process.env.PUBLIC_PATH,
            },
        };
    }

    return {};
};

exports.plugins = [
    ui(),
];
