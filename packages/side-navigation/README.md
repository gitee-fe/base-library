# 侧边导航组件

## 属性列表

```ts
type SideNavigationState = 'collapsed' | 'expanded'

interface SideNavigationProps {
    logo: ReactElement; // 显示Logo，必须是Logo组件
    title: string; // 系统名称
    setting: boolean; // 是否显示设置按钮
    children?: ReactElement[]; // 导航内容，必须是Category组件
    defaultState?: SideNavigationState; // 展开或收起状态
}

interface LogoProps {
    collapsedImage: string; // 收起时的图片路径
    expandedImage: string; // 展开时的图片路径
}

interface CategoryProps {
    icon: ReactElement; // 图标
    title: string; // 标题
    children: ReactElement[]; // 二级菜单，必须是Item组件
}

interface ItemProps {
    to: string; // 路转的URL
    title: string; // 标题
}
```

## 使用

```jsx
<SideNavigation
    setting
    defaultState="expanded"
    logo={<Logo collapsedImage={collapsedLogo} expandedImage={expandedLogo} />}
    title="效率云"
>
    <Category title="iCode" icon={<CodeSandboxOutlined />}>
        <Item to="/foo" title="代码库" />
        <Item to="/foo" title="评审" />
    </Category>
    <Category title="iCafe" icon={<CoffeeOutlined />}>
        <Item to="/foo" title="计划跟踪" />
        <Item to="/foo" title="卡片查询" />
        <Item to="/foo" title="报表分析" />
    </Category>
    <Category title="iScan" icon={<SecurityScanOutlined />}>
        <Item to="/foo" title="最近扫描" />
        <Item to="/foo" title="任务查询" />
    </Category>
    <Category title="iPipe" icon={<CiOutlined />}>
        <Item to="/foo" title="构建历史" />
        <Item to="/foo" title="任务配置" />
    </Category>
</SideNavigation>
```

具体可以参考`demo/entries/index.js`文件。

使用`yarn start`启动测试页面即可调试。
