import React, {FC, ReactElement, cloneElement, useState, useRef, useEffect} from 'react';
import classNames from 'classnames';
import {useToggle} from '@huse/boolean';
import {useHover} from '@huse/hover';
import {useSideNavigationState} from '../Provider';
import './index.less';

interface Props {
    icon: ReactElement;
    title: string;
}

const SideNavigationCategory: FC<Props> = ({icon, title, children}) => {
    const state = useSideNavigationState();
    const [expanded, toggle] = useToggle(false);
    const [top, setTop] = useState(0);
    const ref = useRef<HTMLDivElement>(null);
    useEffect(
        () => {
            const top = ref.current?.getBoundingClientRect().top ?? 0;
            setTop(top);
        },
        []
    );
    const [hover, hoverCallbacks] = useHover({delay: 180});

    return (
        <div ref={ref} className={classNames('category', state)} {...(state === 'collapsed' ? hoverCallbacks : {})}>
            <div
                className={classNames('category-main', {active: state === 'expanded' && expanded})}
                onClick={state === 'expanded' ? toggle : undefined}
            >
                {cloneElement(icon, {className: 'category-icon'})}
                {state === 'expanded' && <span className="category-text">{title}</span>}
            </div>
            {
                state === 'collapsed' && hover && (
                    <div className="category-content" style={{top}}>
                        <span className="category-content-title">{title}</span>
                        {children}
                    </div>
                )
            }
            {state === 'expanded' && expanded && children}
        </div>
    );
};

export default SideNavigationCategory;
