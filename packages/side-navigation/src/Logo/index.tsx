import React, {FC} from 'react';
import {useSideNavigationState} from '../Provider';
import './index.less';

interface Props {
    collapsedImage: string;
    expandedImage: string;
}

const Logo: FC<Props> = ({collapsedImage, expandedImage}) => {
    const state = useSideNavigationState();
    return (
        <div className="logo">
            <img src={state === 'collapsed' ? collapsedImage : expandedImage} />
        </div>
    );
};

export default Logo;
