import React, {createContext, useState, useCallback, useMemo, FC, useContext} from 'react';

export type SideNavigationState = 'collapsed' | 'expanded';

export interface SideNavigationContext {
    state: SideNavigationState;
    onToggle(): void;
}

const DEFAULT_CONTEXT_VALUE: SideNavigationContext = {
    state: 'collapsed',
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    onToggle() {
    },
};

const Context = createContext(DEFAULT_CONTEXT_VALUE);
Context.displayName = 'SideNavigationContext';

interface Props {
    defaultState?: SideNavigationState;
}

const SideNavigationProvider: FC<Props> = ({defaultState = 'collapsed', children}) => {
    const [state, setState] = useState<SideNavigationState>(defaultState);
    const toggle = useCallback(
        () => setState(state => (state === 'collapsed' ? 'expanded' : 'collapsed')),
        []
    );
    const contextValue: SideNavigationContext = useMemo(
        () => ({state, onToggle: toggle}),
        [state, toggle]
    );

    return (
        <Context.Provider value={contextValue}>
            {children}
        </Context.Provider>
    );
};

export default SideNavigationProvider;

export const useSideNavigationState = () => {
    const {state} = useContext(Context);
    return state;
};

export const useToggleSideNavigation = () => {
    const {onToggle} = useContext(Context);
    return onToggle;
};
