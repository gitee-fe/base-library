import React, {FC, createElement} from 'react';
import classNames from 'classnames';
import {MenuFoldOutlined, MenuUnfoldOutlined} from '@ant-design/icons';
import {useSideNavigationState, useToggleSideNavigation} from '../../Provider';
import './index.less';

const SideNavigationToggle: FC = () => {
    const state = useSideNavigationState();
    const toggle = useToggleSideNavigation();

    return (
        <div className={classNames('toggle', state)} onClick={toggle}>
            {createElement(state === 'collapsed' ? MenuUnfoldOutlined : MenuFoldOutlined)}
        </div>
    );
};

export default SideNavigationToggle;
