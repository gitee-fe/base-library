import React, {FC} from 'react';
import classNames from 'classnames';
import {useSideNavigationState} from '../Provider';
import Setting from './Setting';
import Toggle from './Toggle';
import './index.less';

interface Props {
    setting: boolean;
}

const SideNavigationFooter: FC<Props> = ({setting}) => {
    const state = useSideNavigationState();

    return (
        <div className={classNames('footer', state)}>
            {setting && <Setting />}
            <Toggle />
        </div>
    );
};

export default SideNavigationFooter;
