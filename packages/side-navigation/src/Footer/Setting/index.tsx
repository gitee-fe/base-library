import React, {FC} from 'react';
import classNames from 'classnames';
import {SettingOutlined} from '@ant-design/icons';
import {useSideNavigationState} from '../../Provider';
import './index.less';

const SideNavigationSetting: FC = () => {
    const state = useSideNavigationState();

    return (
        <div className={classNames('setting', state)}>
            <SettingOutlined />
            {state === 'expanded' && <span className="setting-text">项目设置</span>}
        </div>
    );
};

export default SideNavigationSetting;
