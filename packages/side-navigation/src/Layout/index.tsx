import React, {ReactElement, FC} from 'react';
import classNames from 'classnames';
import {useSideNavigationState} from '../Provider';
import Footer from '../Footer';
import './index.less';

export interface Props {
    logo: ReactElement;
    title: string;
    setting: boolean;
    children?: ReactElement[];
}

const SideNavigationLayout: FC<Props> = ({logo, title, setting, children}) => {
    const state = useSideNavigationState();

    return (
        <header className={classNames('side-navigation', state)}>
            <div className={classNames('phantom', state)} />
            <div className={classNames('layout', state)}>
                {logo}
                <div className="layout-title">
                    {title}
                </div>
                <nav className="layout-main">
                    {children}
                </nav>
                <Footer setting={setting} />
            </div>
        </header>
    );
};

export default SideNavigationLayout;
