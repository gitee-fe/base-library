import React, {FC} from 'react';
import Provider, {SideNavigationState} from './Provider';
import Layout, {Props as LayoutProps} from './Layout';

interface Props extends LayoutProps {
    defaultState?: SideNavigationState;
}

export const SideNavigation: FC<Props> = ({defaultState, ...props}) => (
    <Provider defaultState={defaultState}>
        <Layout {...props} />
    </Provider>
);

export {default as Logo} from './Logo';
export {default as Category} from './Category';
export {default as Item} from './Item';
