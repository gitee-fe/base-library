import React, {FC} from 'react';
import {NavLink} from 'react-router-dom';
import classNames from 'classnames';
import {useSideNavigationState} from '../Provider';
import './index.less';

interface Props {
    to: string;
    title: string;
}

const SideNavigationItem: FC<Props> = ({to, title}) => {
    const state = useSideNavigationState();

    return (
        <NavLink className={classNames('item', state)} activeClassName="active" to={to}>
            {title}
        </NavLink>
    );
};

export default SideNavigationItem;
