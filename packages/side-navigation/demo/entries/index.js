import {render} from 'react-dom';
import {CodeSandboxOutlined, CoffeeOutlined, SecurityScanOutlined, CiOutlined} from '@ant-design/icons';
import '@gitee/ui/theme.less';
import {MemoryRouter} from 'react-router-dom';
import {SideNavigation, Logo, Category, Item} from '../../src';
import collapsedLogo from './logo-collapsed.svg';
import expandedLogo from './logo-expanded.svg';

render(
    <MemoryRouter>
        <SideNavigation
            setting
            defaultState="expanded"
            logo={<Logo collapsedImage={collapsedLogo} expandedImage={expandedLogo} />}
            title="效率云"
        >
            <Category title="iCode" icon={<CodeSandboxOutlined />}>
                <Item to="/1" title="代码库" />
                <Item to="/2" title="评审" />
            </Category>
            <Category title="iCafe" icon={<CoffeeOutlined />}>
                <Item to="/3" title="计划跟踪" />
                <Item to="/4" title="卡片查询" />
                <Item to="/5" title="报表分析" />
            </Category>
            <Category title="iScan" icon={<SecurityScanOutlined />}>
                <Item to="/6" title="最近扫描" />
                <Item to="/7" title="任务查询" />
            </Category>
            <Category title="iPipe" icon={<CiOutlined />}>
                <Item to="/8" title="构建历史" />
                <Item to="/9" title="任务配置" />
            </Category>
        </SideNavigation>
    </MemoryRouter>,
    document.body.appendChild(document.createElement('div'))
);
