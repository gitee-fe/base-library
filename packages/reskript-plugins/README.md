# reSKRipt插件

服务于[reskript](https://www.npmjs.com/package/reskript)构建的一些常用插件。

## 安装

```shell
yarn add -D @gitee/reskript-plugins
```

## 插件

### 集成UI

与`gitee/ui-theme`进行自动化的集成，自动引入相关的样式并覆盖`antd`。

```js
// settings.js
const {ui} = require('@gitee/reskript-plugins');

exports.plugins = [
    ui(),
];
```

这个插件负责将`ui-theme`指定的用于覆盖`antd`样式的文件注入到`styleResources`配置中。
