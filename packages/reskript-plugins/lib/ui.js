module.exports = () => settings => {
    const {build: {styleResources = []}} = settings;
    return {
        ...settings,
        build: {
            ...settings.build,
            styleResources: [
                ...styleResources,
                require.resolve('@gitee/ui-theme/overrides.less'),
                require.resolve('@gitee/ui-theme/antd.less'),
            ],
        },
    };
};
