const path = require('path');
const fs = require('fs');
const baidu = require('./baidu');
const gitee = require('./gitee');

const formatToVariables = (colors, prefix) => {
    const variables = {
        ...colors.tagColors,
        ...colors.uiColors,
    };
    const content = Object.entries(variables).map(([name, value]) => `${prefix}${name.slice(2)}: ${value};`).join('\n');
    return content + '\n';
};

const formatToCSS = colors => {
    const variables = {
        ...colors.tagColors,
        ...colors.rotatingColors,
        ...colors.uiColors,
        ...colors.presetColors,
    };
    const content = Object.entries(variables).map(([name, value]) => `    ${name}: ${value};`).join('\n');
    return `:root {\n${content}\n}\n`;
};

const generateStyles = product => {
    const destination = path.join(__dirname, '..', product);
    const colors = product === 'gitee' ? gitee : baidu;
    fs.rmdirSync(destination, {recursive: true});
    fs.mkdirSync(destination);
    fs.writeFileSync(path.join(destination, 'vars.less'), formatToVariables(colors, '@'));
    fs.writeFileSync(path.join(destination, 'vars.scss'), formatToVariables(colors, '$'));
    fs.writeFileSync(path.join(destination, 'vars.css'), formatToCSS(colors));
};

generateStyles('baidu');
generateStyles('gitee');
