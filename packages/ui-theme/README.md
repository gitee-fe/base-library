# UI主题库

## 为什么要独立

`@gitee/ui`是一个基于`antd`的封装。在效率云和gitee的合作过程中，还存在不少基于`element-ui`、`SCSS`这一套技术的产品。

为了统一所有产品的颜色、样式变量等内容，我们需要一个单独的库来管理纯样式的信息，不与`antd`耦合。

## 使用

### 安装

```shell
yarn add @gitee/ui-theme
```

### 引用变量

有`gitee`和`baidu`两个目录，内部分别有`vars.scss`、`vars.less`和`vars.css`文件。

其中`vars.scss`和`vars.less`包含了色盘的定义，可直接用于SCSS和LESS。

`vars.css`中是所有颜色的CSS变量，定义在`:root`选择器下。除了色盘颜色外，还包括了如`--color-primary`等预设颜色，以及`--rotating-color-(1-10)`的轮换色（用于标签、图表等场景）。

**建议在可行的场合直接使用CSS变量，避免使用LESS和SCSS变量。**

使用SCSS：

```scss
@import "~@gitee/ui-theme/gitee/vars.scss";
```

使用LESS：

```less
@import "~@gitee/ui-theme/baidu/vars.less";
```

引入CSS变量（在JS入口）：

```js
import '@gitee/ui-theme/baidu/vars.css';
```

### 覆盖antd

`antd`有自己的一套色盘逻辑，为了让我们的色盘可以覆盖它，需要应用对`antd`的样式覆盖，为此要在所有的`.less`文件中插入`overrides.less`（覆盖一些颜色计算的函数）和`antd.less`（变量覆盖）两个文件，以下工作**仅对LESS有效**。

如果使用[reskript](https://www.npmjs.com/package/reskript)进行构建，可以在`settings.js`里增加以下内容：

```js
exports.build = {
    styleResources: [
        require.resolve('@gitee/ui-theme/overrides.less'),
        require.resolve('@gitee/ui-theme/vars.less'),
    ],
};
```

如果自定义`webpack`的配置，则使用[style-resources-loader](https://www.npmjs.com/package/style-resources-loader)以`append`模式来引入以上2个文件。

### 示例

查看`ui-site`可以得到较为完整的`ui-theme`与`ui`包使用的范例。
