# 基础UI库

本代码库导出与`antd`完全一致的组件集。

## 使用

在使用组件之前，参考`ui-theme`包的说明将CSS变量引入。

在具体产品中，不要直接引用`antd`的代码，即按照以下规则修改：

```typescript
// 原有引入antd的代码
import {Button, DatePicker} from 'antd';
// 修改包名引入@gitee/ui
import {Button, DatePicker} from '@gitee/ui';
```

`@gitee/ui`确保导出与`antd`相同的内容（除`version`外）。

## 维护代码库

### 确保样式

由于原本`antd`的组件引入是有一些技巧的：

1. 如果直接使用`import {Button} from 'antd'`是没有样式的。
2. 如果增加`import 'antd/dist/ant.min.css`会引入一个很大的样式文件，哪怕你只使用一个组件。
3. 官方推荐使用[babel-plugin-import](https://www.npmjs.com/package/babel-plugin-import)来解决问题，但`tsc`并不支持。
4. 如果使用`babel`来编译TypeScript文件，会出现部分兼容性问题。

因此，在维护这个代码库的时候，为了让`tsc`正常工作，我们需要**手工地进行样式引入**。

对于任何`antd`提供的组件，以`Button`为例，需要把代码写成这样：

```typescript
import Button from 'antd/es/button';
import 'antd/es/button/style';
```

每一个组件都对应2行代码，缺一不可。

### 覆盖样式

除了`styles/vars/.less`中对整体变量的覆盖外，如果有需要对某个组件中的具体CSS选择器进行覆盖，则按以下步骤：

1. 在`src/`下对应组件目录下新建`index.less`。
2. 在`index.less`中增加相应CSS选择器的覆盖。
3. 在对应的`index.ts`中增加`import './index.less';`语句引入样式。
