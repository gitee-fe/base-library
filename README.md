# base-library

## 介绍

Gitee FE 基础库。

## 代码组织

使用[lerna](https://github.com/lerna/lerna)进行多模块的管理，使用[yarn](https://yarnpkg.com/)进行依赖管理。

所有的模块放置在`/packages/`下，不进行发布的包（如演示用模块）在`package.json`中增加`"private": true`字段即可。

### 模块要求

#### 模块名

模块对应的文件夹名字能说明模块的作用，如`ui`表示组件库，`icon`表示图标库等。

`package.json`中的模块名采用`@gitee/*`的方式，`@gitee/`后的部分与文件夹名保持一致。

建议模块初始版本为`0.8.0`，由lerna管理版本发布，稳定时发布到`1.0.0`版本。

所有模块必须在`package.json`下的`scripts`字段中声明以下脚本：

- `lint`：进行代码规范检查。
- `build`：进行一次完整的构建。
- `build-check`：进行快速的构建测试，如使用`tsc`做类型检查。这一命令的作用是确认构建是否能够通过，并不要求一定有构建产物。
- `test`：进行单元或集成测试。

以上命令失败时退出码不为`0`即可。

### 代码提交

统一使用[约定式提交](https://www.conventionalcommits.org/zh-hans/v1.0.0-beta.4/)，常用以下前缀：

- `feat:`表示有新功能增加。
- `fix:`表示修复BUG。
- `docs:`表示补充文档和说明等内容。
- `build:`表示修改构建脚本等。
- `test:`表示增加或修改测试。
- `ci:`表示修改CI/CD相关内容，如代码检查等。
- `chore:`表示日常维护，如升级依赖等。
- `refractor:`表示对功能和BUG没有影响的内部重构，包括增加注释等。

### 编码规范

暂时统一使用[@ecomfe/eslint-config](https://www.npmjs.com/package/@ecomfe/eslint-config)作为规则集，由[reskript](https://www.npmjs.com/package/reskript)提供检测能力，后续讨论后再调整相应的规则。

具体当前的编码规范参考：https://github.com/ecomfe/spec

## 发布

### 更新版本

在**确保没有未提交文件**的情况下，使用`yarn release`发布包，过程中会提示所有包的更新前后版本，确认后继续，会生成相应的提交记录。

通过`git log`确认提交记录正常，随后`git push --follow-tags`将最新的代码推送至远端。

**注意：更新版本的操作仅能在`master`分支上操作，且操作完后务必不要再更改提交历史，否则会导致后续的版本更新失败。**

### 发布包

使用`lerna publish from-package --registry=xxx`命令发布包，选择正确的镜像发布即可。

## 包索引

| 包名 | 作用 |
| --- | --- |
| ui | 导出与`antd`一致的组件 |
| ui-theme | 提供色盘变量、CSS变量，覆盖`antd`样式 |
| side-navigation | 侧边导航组件 |
| reskript-plugins | 用于[reskript](https://www.npmjs.com/package/reskript)构建的插件 |
| ui-site | 组件演示用站点 |
